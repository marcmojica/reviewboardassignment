﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPATest.Model
{
    
    public class ReviewBoard
    {
        private const int MAX_LIBERAL_ARTS_SCHOOL = 5;
        private const int MAX_MUSIC_SCHOOL = 3;

        private const string GPA_TOO_LOW = "GPA too low";
        private const string SAT_VERBAL_TOO_LOW = "SAT Verbal too low";
        private const string SAT_MATH_TOO_LOW = "SAT Math too low";
        private const string TOTAL_SAT_TOO_LOW = "Total SAT Score too low";
        private const string MAX_ACCEPTANCE = "School is full";

        public int MusicSchoolAccepted
        {
            get
            {
                var total = 0;

                foreach (var record in this.RecordsReviewed)
                {
                    if (record.ReviewStatus == ReviewStatusType.Accepted && record.School == SchoolType.Music)
                        total++;
                }

                return total;
            }
        }

        public int MusicSchoolRejected
        {
            get
            {
                var total = 0;

                foreach (var record in this.RecordsReviewed)
                {
                    if (record.ReviewStatus == ReviewStatusType.Rejected && record.School == SchoolType.Music)
                        total++;
                }

                return total;
            }
        }

        public int LiberalArtsAccepted
        {
            get
            {
                var total = 0;

                foreach (var record in this.RecordsReviewed)
                {
                    if (record.ReviewStatus == ReviewStatusType.Accepted && record.School == SchoolType.LiberalArts)
                        total++;
                }

                return total;
            }
        }

        public int LiberalArtsRejected
        {
            get
            {
                var total = 0;

                foreach (var record in this.RecordsReviewed)
                {
                    if (record.ReviewStatus == ReviewStatusType.Rejected && record.School == SchoolType.LiberalArts)
                        total++;
                }

                return total;
            }
        }

        public List<StudentRecord> RecordsReviewed { get; set; }

        public int TotalAccepted
        {
            get
            {
                var total = 0;

                foreach (var record in this.RecordsReviewed)
                {
                    if (record.ReviewStatus == ReviewStatusType.Accepted)
                        total++;
                }
               
                return total;
            }
        }


        public int LiberalArtsReviewedTotal
        {
            get
            {
                var total = 0;

                foreach (var record in this.RecordsReviewed)
                {
                    if (record.School == SchoolType.LiberalArts)
                        total++;
                }

                return total;
            }
        }

        public int MusicSchoolReviewedTotal
        {
            get
            {
                var total = 0;

                foreach (var record in this.RecordsReviewed)
                {
                    if (record.School == SchoolType.Music)
                        total++;
                }

                return total;
            }
        }

        public int TotalRejected
        {
            get
            {
                var total = 0;

                foreach (var record in this.RecordsReviewed)
                {
                    if (record.ReviewStatus == ReviewStatusType.Rejected)
                        total++;
                }

                return total;
            }
        }

        public ReviewBoard()
        {
            RecordsReviewed = new List<StudentRecord>();
        }

        public void ReviewApplicants(List<StudentRecord> records)
        {
            foreach (var record in records)
            {
                this.ReviewApplicant(record);
            }
        }

        public ReviewStatusType ReviewApplicant(StudentRecord record)
        {
            // reset the rejection reasons
            record.RejectionReasons = new List<string>();

            // start the review
            switch(record.School)
            {
                case SchoolType.LiberalArts:
                    record.ReviewStatus = this.IsEligibleForLiberalArtsSchool(record) ? ReviewStatusType.Accepted : ReviewStatusType.Rejected;
                    break;
                case SchoolType.Music:
                    record.ReviewStatus = this.IsEligibleForMusicSchool(record) ? ReviewStatusType.Accepted : ReviewStatusType.Rejected;
                    break;
                default:
                    throw new Exception("Bad Record");
            }

            this.RecordsReviewed.Add(record);

            return record.ReviewStatus;
        }



        /// <summary>
        /// No preferences for alumni here
        /// Math and verbal SAT’s must be at least 500.
        /// </summary>
        /// <returns></returns>
        public bool IsEligibleForMusicSchool(StudentRecord record)
        {
            // assume eligible until otherwise not
            bool result = true;

            // we test each scenario separately because we need to record rejection reason

            // Test to see if max accepted already; first come, first servered
            if (this.MusicSchoolAccepted == MAX_MUSIC_SCHOOL)
            {
                result = false;
                record.RejectionReasons.Add(MAX_ACCEPTANCE);
            }

            /// Math SAT must be at least 500.
            if (record.SatMathScore < 500)
            {
                result = false;
                record.RejectionReasons.Add(SAT_MATH_TOO_LOW);
            }

            /// Verbal SAT must be at least 500.
            if (record.SatVerbalScore < 500)
            {
                result = false;
                record.RejectionReasons.Add(SAT_VERBAL_TOO_LOW);
            }

            return result;
        }

        /// <summary>
        /// If a parent is an alumnus, the GPA must be at least 3.0, but if no parents are alumni the GPA must be at least 3.5.
        /// If a parent is an alumnus, the combined SAT score must be at least 1000, but if no parents are alumni the SAT must be at least 1200.
        /// </summary>
        /// <returns></returns>
        public bool IsEligibleForLiberalArtsSchool(StudentRecord record)
        {
            // assume eligible until otherwise not
            bool result = true;

            // we test each scenario separately because we need to record rejection reason

            // Test to see if max accepted already; first come, first servered
            if (this.LiberalArtsAccepted == MAX_LIBERAL_ARTS_SCHOOL)
            {
                result = false;
                record.RejectionReasons.Add(MAX_ACCEPTANCE);
            }

            if (record.ParentAlumnus)
            {
                // If a parent is an alumnus, the combined SAT score must be at least 1000
                if (record.TotalSatScore < 1000)
                {
                    result = false;
                    record.RejectionReasons.Add(TOTAL_SAT_TOO_LOW);
                }

                // if a parent is an alumnus, the GPA must be at least 3.0
                if (record.HighSchoolGradePointAverage < 3.0)
                {
                    result = false;
                    record.RejectionReasons.Add(GPA_TOO_LOW);
                }
            }
            else 
            {
                // if no parents are alumni the SAT must be at least 1200.
                if (record.TotalSatScore < 1200)
                {
                    result = false;
                    record.RejectionReasons.Add(TOTAL_SAT_TOO_LOW);
                }

                // if no parents are alumni the GPA must be at least 3.5
                if (record.HighSchoolGradePointAverage < 3.5)
                {
                    result = false;
                    record.RejectionReasons.Add(GPA_TOO_LOW);
                }
            }

            return result;
        }
        

       
    }
}
