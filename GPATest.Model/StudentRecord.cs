﻿using System;
using System.Collections.Generic;
using System.IO;

namespace GPATest.Model
{
    public class StudentRecord
    {
        public int Id { get; set; }

        public SchoolType School { get; set; }

        
        public string SchoolName
        {
            get
            {
                switch (this.School)
                {
                    case SchoolType.LiberalArts:
                        return "Liberal Arts";
                        
                    case SchoolType.Music:
                        return "Music";
                    default:
                        throw new Exception("School Type not Specified");
                }
            }
        }
        
        public float HighSchoolGradePointAverage { get; set; }

        public int SatMathScore { get; set; }

        public int SatVerbalScore { get; set; }

        public bool ParentAlumnus { get; set; }

        public int TotalSatScore
        {
            get
            {
                return SatMathScore + SatVerbalScore;
            }
        }


        public ReviewStatusType ReviewStatus { get; set; }

        public List<string> RejectionReasons { get; set; }

        public StudentRecord()
        {
            RejectionReasons = new List<string>();
            ReviewStatus = ReviewStatusType.NotReviewed;
        }

        

        /// <summary>
        /// Data is exactly same size; we can reference values directly from data
        /// Use split because it's easier, no need to split data
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        private static StudentRecord CreateStudentRacordUsingSplit(string rawData)
        {
            //L 4.0 600 650 N
            var data = rawData.Split(' ');

            var record = new StudentRecord()
            {
                School = data[0][0] == 'L' ? SchoolType.LiberalArts : SchoolType.Music,
                HighSchoolGradePointAverage = float.Parse(data[1]),
                SatMathScore = int.Parse(data[2]),
                SatVerbalScore = int.Parse(data[3]),
                ParentAlumnus = data[4][0] == 'Y'
            };

            return record;
        }
        
        /// <summary>
        /// Data is exactly same size; we can reference values directly from data
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        private static StudentRecord CreateStudentRacordUsingSubstring(string rawData)
        {
            //L 4.0 600 650 N

            var school = rawData.Substring(0, 1)[0] == 'L' ? SchoolType.LiberalArts : SchoolType.Music;
            var gradePointAverage = float.Parse(rawData.Substring(2, 3));
            var satMathScore = int.Parse(rawData.Substring(6, 3));
            var satVerbalScore = int.Parse(rawData.Substring(10, 3));
            var alumnus = rawData.Substring(14, 1)[0] == 'Y';
            
            var record = new StudentRecord()
            {
                School = school,
                HighSchoolGradePointAverage = gradePointAverage,
                SatMathScore = satMathScore,
                SatVerbalScore = satVerbalScore,
                ParentAlumnus = alumnus
            };

            return record;
        }
        

        /// <summary>
        /// Load data from file specified
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static List<StudentRecord> Load(string file)
        {
            string[] lines = File.ReadAllLines(file);

            // load and transform data to create all the records         
            var records = new List<StudentRecord>();
            var count = 1;

            foreach (string line in lines)
            {
                var record = StudentRecord.CreateStudentRacordUsingSplit(line);
                //var record = StudentRecord.CreateStudentRacordUsingSubstring(line);

                // set id
                record.Id = count;
                count++;

                records.Add(record);
            }

            return records;
        }

        /// <summary>
        /// custom ToString. Useful for debug purposes
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            //School = L GPA = 3.8 math = 590 verbal = 600 alumnus = N

            return $"School = {(this.School == SchoolType.LiberalArts ? 'L' : 'M')} GPA = {this.HighSchoolGradePointAverage} math = {this.SatMathScore} verbal = {this.SatVerbalScore} alumnus {(this.ParentAlumnus ? 'Y' : 'N')}";
        }
    }
}

