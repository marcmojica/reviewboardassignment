﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPATest.Model
{
    public interface IReviewBoardOutputGenerator
    {
        string Output(ReviewBoard reviewBoard);
    }
}
