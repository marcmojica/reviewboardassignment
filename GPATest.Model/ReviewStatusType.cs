﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPATest.Model
{
    public enum ReviewStatusType
    {
        NotReviewed,
        Accepted,
        Rejected
    }
}
