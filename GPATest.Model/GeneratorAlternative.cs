﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPATest.Model
{
    public class GeneratorAlternative : IReviewBoardOutputGenerator
    {
        public string Output(ReviewBoard reviewBoard)
        {
            var builder = new StringBuilder();

            builder.AppendLine("Applicants Reviewed by Marc Mojica");

            builder.AppendLine("*******************************");

            foreach (var record in reviewBoard.RecordsReviewed)
            {
                builder.Append($"Applicant #: {record.Id}");
                builder.AppendLine($" - High School GPA: {String.Format("{0:0.0}",record.HighSchoolGradePointAverage)} | SAT Math Score: {record.SatMathScore} | SAT Verbal Score: = {record.SatVerbalScore} {(record.ParentAlumnus ? " | *** Alumnus ***" : String.Empty )}");

                switch (record.ReviewStatus)
                {
                    case ReviewStatusType.Accepted:
                        builder.AppendLine($"Status: ACCEPTED to {record.SchoolName} College");
                        break;
                    case ReviewStatusType.Rejected:
                        builder.AppendLine($"Status: REJECTED to {record.SchoolName} College");
                        foreach (var reason in record.RejectionReasons)
                        {

                            builder.AppendLine($"Reason: {reason}");
                        }
                        break;
                }

                builder.AppendLine("*******************************");
            }

            builder.AppendLine($"There were {reviewBoard.TotalAccepted} of {reviewBoard.RecordsReviewed.Count} applicants accepted");
            builder.AppendLine($"There were {reviewBoard.LiberalArtsAccepted} of {reviewBoard.LiberalArtsReviewedTotal} accepted to Liberal Arts College");
            builder.AppendLine($"There were {reviewBoard.MusicSchoolAccepted} of {reviewBoard.MusicSchoolReviewedTotal} accepted to Music College");


            return builder.ToString();
        }
    }
}
