﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPATest.Model
{
    public class GeneratorAssignment : IReviewBoardOutputGenerator
    {
        public string Output(ReviewBoard reviewBoard)
        {
            var builder = new StringBuilder();

            // display, no logic here
            builder.AppendLine("Acceptance to College by Marc Mojica");

            foreach (var record in reviewBoard.RecordsReviewed)
            {
                builder.AppendLine($"Applicant #: {record.Id}");
                builder.AppendLine($"School = {(record.School == SchoolType.LiberalArts ? 'L' : 'M')} GPA = {String.Format("{0:0.0}", record.HighSchoolGradePointAverage)} math = {record.SatMathScore} verbal = {record.SatVerbalScore} alumnus {(record.ParentAlumnus ? 'Y' : 'N')}");

                builder.AppendLine($"Applying to {record.SchoolName}");

                switch (record.ReviewStatus)
                {
                    case ReviewStatusType.Accepted:
                        builder.AppendLine($"Accepted to {record.SchoolName}!!");
                        break;
                    case ReviewStatusType.Rejected:
                        foreach (var reason in record.RejectionReasons)
                        {
                            builder.AppendLine($"Rejected - {reason}");
                        }
                        break;
                }

                builder.AppendLine("*******************************");
            }

            builder.AppendLine($"There were {reviewBoard.RecordsReviewed.Count} in the file");
            builder.AppendLine($"There were {reviewBoard.LiberalArtsAccepted} acceptances to Liberal Arts");
            builder.AppendLine($"There were {reviewBoard.MusicSchoolAccepted} acceptances to Music");
            builder.AppendLine();
            builder.AppendLine("Press any key to continue");

            return builder.ToString();
        }
    }
}
