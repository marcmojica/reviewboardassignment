﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPATest.Model
{
    public class GeneratorHtml : IReviewBoardOutputGenerator
    {
        public string Output(ReviewBoard reviewBoard)
        {
            var builder = new StringBuilder();
            builder.AppendLine("<html>");
            builder.AppendLine("<head>");
            builder.AppendLine("\t<title>Review Board Results</title>");
            builder.AppendLine("\t<style>");
            builder.AppendLine("\ttable { border: 1px solid grey; padding: 0; margin:0; } td, th { border-left: 0; border-right: 0; border-top: 0; border-bottom: 1px solid black; margin: 0; padding: 5px; }");
            builder.AppendLine("\t.accepted { background-color: LightCyan; } .rejected { background-color: LightPink; }");

            builder.AppendLine("\t</style>");
            builder.AppendLine("</head>");
            builder.AppendLine("<body>");
            builder.AppendLine("\t<h1>Applicants Reviewed by <a href=\"http://www.marcmojica.com\" target=\"_blank\">Marc Mojica</a></h1>");
            builder.AppendLine("\t<hr />");
            builder.AppendLine("\t<h2>Summary</h2>");
            builder.AppendLine($"\t<div>{reviewBoard.TotalAccepted} of {reviewBoard.RecordsReviewed.Count} applicants accepted</div>");
            builder.AppendLine($"\t<div>{reviewBoard.LiberalArtsAccepted} of {reviewBoard.LiberalArtsReviewedTotal} accepted to Liberal Arts College</div>");
            builder.AppendLine($"\t<div>{reviewBoard.MusicSchoolAccepted} of {reviewBoard.MusicSchoolReviewedTotal} accepted to Music College</div>");

            builder.AppendLine("\t<hr />");
            builder.AppendLine("\t<h2>Records</h2>");
            builder.AppendLine("\t<table cellpadding=\"0\" cellspacing=\"0\">");
            builder.AppendLine("\t\t<tr>");

            builder.AppendLine("\t\t\t<th>#</th>");
            builder.AppendLine("\t\t\t<th>College</th>");
            builder.AppendLine("\t\t\t<th>GPA</th>");
            builder.AppendLine("\t\t\t<th>SAT Math</th>");
            builder.AppendLine("\t\t\t<th>SAT Verbal</th>");
            builder.AppendLine("\t\t\t<th>Alumnus</th>");
            builder.AppendLine("\t\t\t<th>Status</th>");
            builder.AppendLine("\t\t\t<th>Comments</th>");

            builder.AppendLine("\t\t</tr>");

            foreach (var record in reviewBoard.RecordsReviewed)
            {
                if (record.ReviewStatus == ReviewStatusType.Rejected)
                    builder.AppendLine("\t\t<tr class=\"rejected\">");
                else
                    builder.AppendLine("\t\t<tr class=\"accepted\">");

                builder.AppendLine($"\t\t\t<td>{record.Id}</td>");
                
                builder.AppendLine($"\t\t\t<td>{record.SchoolName}</td>");
                builder.AppendLine($"\t\t\t<td>{String.Format("{0:0.0}", record.HighSchoolGradePointAverage)}</td>");
                builder.AppendLine($"\t\t\t<td>{record.SatMathScore}</td>");
                builder.AppendLine($"\t\t\t<td>{record.SatVerbalScore}</td>");
                builder.AppendLine($"\t\t\t<td>{(record.ParentAlumnus ? "Yes" : "No")}</td>");
                builder.AppendLine($"\t\t\t<td>{(record.ReviewStatus == ReviewStatusType.Accepted ? "Accepted" : "Rejected")}</td>");
                builder.Append($"\t\t\t<td>");
                if (record.ReviewStatus == ReviewStatusType.Rejected)
                {
                    builder.Append($"{string.Join(",", record.RejectionReasons)}");
                }
                builder.AppendLine($"</td>");
                
                
                builder.AppendLine("\t\t</tr>");
            }
            builder.AppendLine("\t</table>");

            builder.AppendLine("</body>");
            builder.AppendLine("</html>");

            return builder.ToString();
        }
    }
}
