Applicants Reviewed by Marc Mojica
*******************************
Applicant #: 1 - High School GPA: 4.0 | SAT Math Score: 600 | SAT Verbal Score: = 650 
Status: ACCEPTED to Liberal Arts College
*******************************
Applicant #: 2 - High School GPA: 3.9 | SAT Math Score: 610 | SAT Verbal Score: = 520 
Status: ACCEPTED to Music College
*******************************
Applicant #: 3 - High School GPA: 3.8 | SAT Math Score: 590 | SAT Verbal Score: = 600 
Status: REJECTED to Liberal Arts College
Reason: Total SAT Score too low
*******************************
Applicant #: 4 - High School GPA: 3.0 | SAT Math Score: 600 | SAT Verbal Score: = 600  | *** Alumnus ***
Status: ACCEPTED to Liberal Arts College
*******************************
Applicant #: 5 - High School GPA: 3.4 | SAT Math Score: 600 | SAT Verbal Score: = 600 
Status: REJECTED to Liberal Arts College
Reason: GPA too low
*******************************
Applicant #: 6 - High School GPA: 3.0 | SAT Math Score: 500 | SAT Verbal Score: = 490  | *** Alumnus ***
Status: REJECTED to Liberal Arts College
Reason: Total SAT Score too low
*******************************
Applicant #: 7 - High School GPA: 2.9 | SAT Math Score: 500 | SAT Verbal Score: = 500  | *** Alumnus ***
Status: REJECTED to Liberal Arts College
Reason: GPA too low
*******************************
Applicant #: 8 - High School GPA: 3.5 | SAT Math Score: 500 | SAT Verbal Score: = 490  | *** Alumnus ***
Status: REJECTED to Music College
Reason: SAT Verbal too low
*******************************
Applicant #: 9 - High School GPA: 3.9 | SAT Math Score: 490 | SAT Verbal Score: = 600  | *** Alumnus ***
Status: REJECTED to Music College
Reason: SAT Math too low
*******************************
Applicant #: 10 - High School GPA: 3.5 | SAT Math Score: 700 | SAT Verbal Score: = 500 
Status: ACCEPTED to Liberal Arts College
*******************************
Applicant #: 11 - High School GPA: 3.1 | SAT Math Score: 600 | SAT Verbal Score: = 400  | *** Alumnus ***
Status: ACCEPTED to Liberal Arts College
*******************************
Applicant #: 12 - High School GPA: 3.0 | SAT Math Score: 490 | SAT Verbal Score: = 510  | *** Alumnus ***
Status: ACCEPTED to Liberal Arts College
*******************************
Applicant #: 13 - High School GPA: 4.0 | SAT Math Score: 800 | SAT Verbal Score: = 800  | *** Alumnus ***
Status: REJECTED to Liberal Arts College
Reason: School is full
*******************************
Applicant #: 14 - High School GPA: 3.2 | SAT Math Score: 500 | SAT Verbal Score: = 500 
Status: ACCEPTED to Music College
*******************************
There were 7 of 14 applicants accepted
There were 5 of 10 accepted to Liberal Arts College
There were 2 of 4 accepted to Music College
