﻿using GPATest.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace GPATest
{
    class Program
    {
        static void Main(string[] args)
        {
            // load data
            var inputFile = @"mp3accept.txt";
            var records = StudentRecord.Load(inputFile);

            
            // now review each record, where the logic happens
            var reviewBoard = new ReviewBoard();
            reviewBoard.ReviewApplicants(records);


            // display output
            // this shows the flexibility of separating logic from display
            var generators = new List<IReviewBoardOutputGenerator>();
            generators.Add(new GeneratorAssignment());
            generators.Add(new GeneratorAlternative());
            generators.Add(new GeneratorAlternative2());
            generators.Add(new GeneratorHtml());

            foreach (var generator in generators)
            {
                Console.WriteLine(generator.Output(reviewBoard));
                Console.ReadKey();
            }

            File.WriteAllText(@"assignmentOutput.txt", generators[0].Output(reviewBoard));
            File.WriteAllText(@"assignmentAlternative.txt", generators[1].Output(reviewBoard));
            File.WriteAllText(@"assignmentAlternative2.txt", generators[2].Output(reviewBoard));
            File.WriteAllText(@"index.html", generators[3].Output(reviewBoard));
        }
    }
}
